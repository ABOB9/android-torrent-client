package com.github.axet.torrentclient.fragments;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.services.TorrentContentProvider;
import com.github.axet.torrentclient.widgets.Pieces;

import libtorrent.InfoTorrent;
import libtorrent.Libtorrent;
import libtorrent.StatsTorrent;

public class InfoFragment extends Fragment implements MainActivity.TorrentFragmentInterface {
    View v;

    Pieces pview;
    TextView size;
    TextView hash;
    TextView pieces;
    TextView creator;
    TextView createdon;
    TextView comment;
    TextView status;
    TextView progress;
    TextView added;
    TextView completed;
    TextView downloading;
    TextView seeding;
    TextView name;
    View pathButton;
    View renameButton;
    ImageView renameButtonView;
    ImageView pathImage;
    ImageView check;
    TextView downloaded;
    TextView uploaded;
    TextView ratio;
    View meta;
    View parts;

    KeyguardManager myKM;

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.torrent_status, container, false);

        final MainActivity main = (MainActivity) getActivity();

        pview = (Pieces) v.findViewById(R.id.torrent_status_pieces);
        size = (TextView) v.findViewById(R.id.torrent_size);
        hash = (TextView) v.findViewById(R.id.torrent_hash);
        pieces = (TextView) v.findViewById(R.id.torrent_pieces);
        creator = (TextView) v.findViewById(R.id.torrent_creator);
        createdon = (TextView) v.findViewById(R.id.torrent_created_on);
        comment = (TextView) v.findViewById(R.id.torrent_comment);
        status = (TextView) v.findViewById(R.id.torrent_status);
        progress = (TextView) v.findViewById(R.id.torrent_progress);
        added = (TextView) v.findViewById(R.id.torrent_added);
        completed = (TextView) v.findViewById(R.id.torrent_completed);
        downloading = (TextView) v.findViewById(R.id.torrent_downloading);
        seeding = (TextView) v.findViewById(R.id.torrent_seeding);
        check = (ImageView) v.findViewById(R.id.torrent_status_check);
        meta = v.findViewById(R.id.torrent_status_metadata);
        parts = v.findViewById(R.id.torrent_status_parts);

        final long t = getArguments().getLong("torrent");

        final String h = Libtorrent.torrentHash(t);
        hash.setText(h);

        pview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FrameLayout f = new FrameLayout(getContext());
                LinearLayout ll = new LinearLayout(getContext());
                ll.setOrientation(LinearLayout.HORIZONTAL);
                final Pieces all = new Pieces(getContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.gravity = Gravity.CENTER;
                ll.addView(all, lp);
                inflater.inflate(R.layout.pieces_legend, ll);
                f.addView(ll, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
                all.cells = (int) Math.ceil(Math.sqrt(Libtorrent.torrentPiecesCount(t)));
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setView(f);
                final AlertDialog d = b.create();
                d.show();
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        if (!d.isShowing())
                            return;
                        all.setTorrent(t);
                        all.postDelayed(this, 1000);
                    }
                };
                run.run();
            }
        });

        View hashCopy = v.findViewById(R.id.torrent_hash_copy);
        if (Build.VERSION.SDK_INT >= 11) {
            hashCopy.setOnClickListener(new View.OnClickListener() {
                @TargetApi(11)
                @Override
                public void onClick(View view) {
                    ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("hash", h);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getContext(), R.string.hash_copied, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hashCopy.setVisibility(View.GONE);
        }

        meta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Libtorrent.downloadMetadata(t)) {
                    ErrorDialog.Error(main, Libtorrent.error());
                    return;
                }
            }
        });

        Storage storage = TorrentApplication.from(getContext()).storage;
        final Uri p = storage.find(t).path;

        TextView path = (TextView) v.findViewById(R.id.torrent_path);
        path.setText(Storage.getDisplayName(getContext(), p) + " "); // PathMax loses forward slash

        pathButton = v.findViewById(R.id.torrent_path_open);
        final Intent intent = storage.openFolderIntent(t); // 'p' can be different from what we open
        if (TorrentContentProvider.isFolderCallable(getContext(), intent)) {
            pathButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main.startActivity(intent);
                }
            });
        } else {
            pathButton.setVisibility(View.GONE);
        }

        name = (TextView) v.findViewById(R.id.torrent_name);

        renameButton = v.findViewById(R.id.torrent_status_rename);
        renameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.renameDialog(t);
            }
        });
        renameButtonView = (ImageView) v.findViewById(R.id.torrent_status_rename_button);

        pathImage = (ImageView) v.findViewById(R.id.torrent_path_image);

        myKM = (KeyguardManager) getContext().getSystemService(Context.KEYGUARD_SERVICE);

        downloaded = (TextView) v.findViewById(R.id.torrent_downloaded);
        uploaded = (TextView) v.findViewById(R.id.torrent_uploaded);
        ratio = (TextView) v.findViewById(R.id.torrent_ratio);

        update();

        return v;
    }

    @Override
    public void update() {
        final long t = getArguments().getLong("torrent");

        final TorrentApplication app = TorrentApplication.from(getContext());
        final Storage storage = app.storage;

        if (myKM.inKeyguardRestrictedInputMode()) {
            pathButton.setEnabled(false);
            pathImage.setColorFilter(Color.GRAY);
        } else {
            pathButton.setEnabled(true);
            pathImage.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
        }

        if (Libtorrent.metaTorrent(t)) {
            meta.setVisibility(View.GONE);
            parts.setVisibility(View.VISIBLE);
            renameButton.setEnabled(true);
            renameButtonView.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
        } else {
            meta.setVisibility(View.VISIBLE);
            parts.setVisibility(View.GONE);
            renameButton.setEnabled(false);
            renameButtonView.setColorFilter(Color.GRAY);
        }

        final Runnable checkUpdate = new Runnable() {
            @Override
            public void run() {
                int s = Libtorrent.torrentStatus(t);
                switch (s) {
                    case Libtorrent.StatusChecking:
                        check.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_stop_black_24dp));
                        check.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
                        check.setEnabled(true);
                        break;
                    case Libtorrent.StatusDownloading:
                    case Libtorrent.StatusQueued:
                    case Libtorrent.StatusSeeding:
                        check.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_done_all_black_24dp));
                        check.setColorFilter(Color.GRAY);
                        check.setEnabled(false);
                        break;
                    default:
                        check.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_done_all_black_24dp));
                        check.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
                        check.setEnabled(true);
                        break;
                }
            }
        };
        checkUpdate.run();
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storage.checkTorrent(t);
                checkUpdate.run();
            }
        });

        pview.setTorrent(t);

        TorrentApplication.setTextNA(name, Libtorrent.torrentName(t));

        TorrentApplication.setTextNA(size, !Libtorrent.metaTorrent(t) ? "" : TorrentApplication.formatSize(getContext(), Libtorrent.torrentBytesLength(t)));

        TorrentApplication.setTextNA(pieces, !Libtorrent.metaTorrent(t) ? "" : Libtorrent.torrentPiecesCount(t) + " / " + TorrentApplication.formatSize(getContext(), Libtorrent.torrentPieceLength(t)));

        InfoTorrent i = Libtorrent.torrentInfo(t);

        TorrentApplication.setTextNA(creator, i.getCreator());

        TorrentApplication.setDate(createdon, i.getCreateOn());

        final String c = i.getComment().trim();
        TorrentApplication.setTextNA(comment, c);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!c.startsWith(WebViewCustom.SCHEME_HTTP))
                    return;

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.open_browser);

                builder.setMessage(c + "\n\n" + getContext().getString(R.string.are_you_sure));
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(c));
                        startActivity(browserIntent);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        switch (Libtorrent.torrentStatus(t)) {
            case Libtorrent.StatusQueued:
                status.setText(R.string.status_queue);
                break;
            case Libtorrent.StatusDownloading:
                status.setText(R.string.status_downloading);
                break;
            case Libtorrent.StatusPaused:
                status.setText(R.string.status_paused);
                break;
            case Libtorrent.StatusSeeding:
                status.setText(R.string.status_seeding);
                break;
            case Libtorrent.StatusChecking:
                status.setText(R.string.status_checking);
                break;
        }

        progress.setText(String.format("%d%%", Storage.Torrent.getProgress(t)));

        StatsTorrent b = Libtorrent.torrentStats(t);
        downloaded.setText(TorrentApplication.formatSize(getContext(), b.getDownloaded()));

        uploaded.setText(TorrentApplication.formatSize(getContext(), b.getUploaded()));

        float r = 0;
        if (Libtorrent.metaTorrent(t)) {
            if (b.getDownloaded() >= Libtorrent.torrentBytesLength(t)) {
                r = b.getUploaded() / (float) b.getDownloaded();
            } else {
                r = b.getUploaded() / (float) Libtorrent.torrentBytesLength(t);
            }
        }
        ratio.setText(String.format("%.2f", r));

        InfoTorrent info = Libtorrent.torrentInfo(t);

        TorrentApplication.setDate(added, info.getDateAdded());

        TorrentApplication.setDate(completed, info.getDateCompleted());

        downloading.setText(TorrentApplication.formatDuration(getContext(), b.getDownloading() / 1000000));

        seeding.setText(TorrentApplication.formatDuration(getContext(), b.getSeeding() / 1000000));
    }

    @Override
    public void close() {

    }
}